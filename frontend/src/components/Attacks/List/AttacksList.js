import { Center } from "@chakra-ui/react";
import Table from "react-bootstrap/Table";

import AttackData from "./AttackData";
import { GoLink, GoAlert, GoCalendar, GoX } from "react-icons/go";
import axios from "axios";

const AttacksList = (props) => {
  return (
    <Center className="m-4 mt-5">
      <Table hover variant="dark" className="lg">
        <thead>
          <tr>
            <th>#</th>
            <th>
              <GoLink /> Address
            </th>
            <th>
              <GoAlert /> Issues
            </th>
            <th>
              <GoCalendar /> Added
            </th>
          </tr>
        </thead>

        <tbody>
          {props.items.length > 0 &&
            props.items.map((atk) => {
              return (
                <AttackData
                  key={atk._id}
                  ip={atk.ip}
                  hostname={atk.hostname}
                  types={atk.types} //atk.types.reduce(atk.types, (prev, curr) => `${prev}, ${curr}`)
                  date={atk.date}
                />
              );
            })}
        </tbody>
      </Table>
    </Center>
  );
};

export default AttacksList;
