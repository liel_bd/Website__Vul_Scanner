import AttackTypesOverlay from '../UI/AttackTypesOverlay'
import { GoTrashcan } from "react-icons/go";
const AttackData = (props) => {
  return (
    <tr>
      <td>#</td>
      <td>{props.hostname}<br/>{props.ip}</td>
      <td><AttackTypesOverlay types={props.types}/></td>
      <td>{props.date}</td>
    </tr>
  );
};

export default AttackData;
