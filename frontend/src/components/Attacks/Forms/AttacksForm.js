import { useState, useEffect } from "react";
import { Form, Button, Col, Row } from "react-bootstrap";
import Modal from "../UI/Modal";
import React from "react";
import { SpinnerDotted } from "spinners-react";
import useInput from "../../../hooks/use-input";
import validator from "validator";
import { GoAlert } from "react-icons/go";

// useEffect(() => {
//     fetchAttacksHandler();
//   }, [fetchAttacksHandler]);

const AttacksForm = (props) => {
  const [isLoading, setIsLoading] = useState(false);

  const {
    value: enteredHostname,
    isValid: enteredHostnameIsValid,
    hasError: hostnameInputHasError,
    valueChangeHandler: hostnameChangeHandler,
    inputBlurHandler: hostnameBlurHandler,
    reset: resetHostnameInput,
  } = useInput(
    (value) =>
      value.trim() != "" &&
      validator.isURL(value) &&
      (value.startsWith("http") || value.startsWith("https"))
  );

  const submitHandler = async (event) => {
    event.preventDefault();

    if (!enteredHostnameIsValid) {
      return;
    }
    setIsLoading(true);
    await props.onAddAttack(enteredHostname);
    setIsLoading(false);
    props.onCloseForm();
    resetHostnameInput();
  };

  return (
    <Modal>
      {/* noValidate validated={validated} */}
      <Form onSubmit={submitHandler} className="m-3 bg-white">
        <Button
          onClick={props.onCloseForm}
          variant="black"
          size="sm"
          className="btn-close position-absolute top-0 start-0"
        />

        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label className="pb-2">Add full hostname address</Form.Label>
          <Form.Control
            placeholder="Enter hostname"
            onChange={hostnameChangeHandler}
            onBlur={hostnameBlurHandler}
            value={enteredHostname}
          />
          {hostnameInputHasError && (
            <div className="text-danger">
              <GoAlert /> Please enter a valid hostname.
            </div>
          )}
        </Form.Group>

        <div className="text-center mt-5">
          {isLoading ? (
            <SpinnerDotted />
          ) : (
            <Button variant="success" type="submit">
              Submit
            </Button>
          )}
        </div>
      </Form>
    </Modal>
  );
};

export default AttacksForm;
