import { useState } from "react";
import { Form } from "react-bootstrap";
import { SpinnerRoundFilled } from "spinners-react";
import BootstrapSwitchButton from "bootstrap-switch-button-react";

const SynFlood = () => {
  const [send, setSend] = useState(true);
  const sendRequests = async() => {
    setSend(!send);
    let url;
    if (send) {
      url = "http://localhost:3001/attacks/synflood/start";
    } else {
      url = "http://localhost:3001/attacks/synflood/stop";
    }
    try {
      await fetch(url);
    } 
    catch (error) {
     console.log(error.message);
    }
  };

  return (
    <Form style={{ marginLeft: "2rem", marginTop: '2rem' }}>
      <p style={{ color: "white" }}>
        <b>SynFlood against defualt router{!send ? <SpinnerRoundFilled size={50} thickness={100} speed={100} color="#36ad47" /> : ''}</b>
      </p>
      <BootstrapSwitchButton
        checked={false}
        onstyle="light"
        offstyle="dark"
        style="border"
        onChange={sendRequests}
      />
    </Form>
  );
};

export default SynFlood;
