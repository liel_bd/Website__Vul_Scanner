import { useState } from "react";
import { Form, Button, Row } from "react-bootstrap";
import Modal from "../UI/Modal";
import React from "react";
import { SpinnerDotted } from "spinners-react";
import useInput from "../../../hooks/use-input";
import validator from "validator";
import { GoAlert } from "react-icons/go";

const SshForm = (props) => {
  const [isLoading, setIsLoading] = useState(false);
  const [file, setFile] = useState("");

  const {
    value: enteredHostname,
    isValid: enteredHostnameIsValid,
    hasError: hostnameInputHasError,
    valueChangeHandler: hostnameChangeHandler,
    inputBlurHandler: hostnameBlurHandler,
    reset: resetHostnameInput,
  } = useInput((value) => value.trim() != "" && validator.isURL(value));

  const {
    value: enteredUsername,
    isValid: enteredUsernameIsValid,
    hasError: usernameInputHasError,
    valueChangeHandler: usernameChangeHandler,
    inputBlurHandler: usernameBlurHandler,
    reset: resetUsernameInput,
  } = useInput((value) => value.trim() != "");

  const {
    value: enteredPort,
    isValid: enteredPortIsValid,
    hasError: portInputHasError,
    valueChangeHandler: portChangeHandler,
    inputBlurHandler: portBlurHandler,
    reset: resetPortInput,
  } = useInput(
    (value) => parseInt(value, 10) > 0 && parseInt(value, 10) < 65535
  );

  const submitHandler = async (event) => {
    event.preventDefault();

    const port =
      enteredPort == ""
        ? 22
        : enteredPortIsValid
        ? parseInt(enteredPort)
        : false;
    if (!enteredUsernameIsValid || !port || !enteredHostnameIsValid) {
      return;
    }
    setIsLoading(true);
    props.onSshScan(enteredHostname, enteredUsername, port, file).then(() => {
      setIsLoading(false);
      props.onCloseSshForm()
    })
  };

  const onUploadFile = (event) => {
    setFile(event.target.files[0]);
  };

  return (
    <Modal>
      {/* noValidate validated={validated} */}
      <Form onSubmit={submitHandler} className="m-3 bg-white">
        <Button
          onClick={props.onCloseSshForm}
          variant="black"
          size="sm"
          className="btn-close position-absolute top-0 start-0"
        />

        <Row className="mb-3">
          <Form.Group className="mt-2 mb-3" controlId="formGridCity">
            <Form.Label>Username</Form.Label>
            <Form.Control
              placeholder="Enter Username"
              onChange={usernameChangeHandler}
              onBlur={usernameBlurHandler}
              value={enteredUsername}
            />
            {usernameInputHasError && (
              <div className="text-danger">
                <GoAlert /> Please enter a valid username.
              </div>
            )}
          </Form.Group>

          <Form.Group className="mb-3" controlId="formGridCity">
            <Form.Label>Hostname</Form.Label>
            <Form.Control
              placeholder="Enter Hostname"
              onChange={hostnameChangeHandler}
              onBlur={hostnameBlurHandler}
              value={enteredHostname}
            />
            {hostnameInputHasError && (
              <div className="text-danger">
                <GoAlert /> Please enter a valid hostname.
              </div>
            )}
          </Form.Group>

          <Form.Group className="mb-3" controlId="formGridCity">
            <Form.Label>Port</Form.Label>
            <Form.Control
              placeholder="Default: 22"
              onChange={portChangeHandler}
              onBlur={portBlurHandler}
              value={enteredPort}
            />
            {portInputHasError && (
              <div className="text-danger">
                <GoAlert /> Please enter a valid port.
              </div>
            )}
          </Form.Group>

          <Form.Group className="mb-2">
            <Form.Label>Passwords (Not required)</Form.Label>
            <Form.Control accept=".txt" onChange={onUploadFile} type="file" />
          </Form.Group>
        </Row>

        <div className="text-center mt-5">
          {isLoading ? (
            <SpinnerDotted />
          ) : (
            <Button variant="success" type="submit">
              Submit
            </Button>
          )}
        </div>
      </Form>
    </Modal>
  );
};

export default SshForm;
