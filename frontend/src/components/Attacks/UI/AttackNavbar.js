import { Navbar, Container, Nav } from "react-bootstrap";
import { FcPlus } from "react-icons/fc";
import { Fragment } from "react";

const NavBar = (props) => {
  return (
    <Fragment>
      <Navbar variant="dark" expand={true}>
        <Container>
          <Nav className="me-auto">
            <Nav.Link onClick={props.onShowForm}>
              <FcPlus /> Scan new target
            </Nav.Link>
            <Nav.Link onClick={props.onShowSshForm}>
              <FcPlus /> Brute-Force SSH Server
            </Nav.Link>
          </Nav>
        </Container>
      </Navbar>
      <hr
        style={{
          color: "white",
          backgroundColor: "white",
          height: 1
        }}
      />
    </Fragment>
  );
};

export default NavBar;
