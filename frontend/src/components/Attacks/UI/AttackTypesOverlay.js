import { useState, useRef } from "react";
import Popover from "react-bootstrap/Popover";
import { Button, Overlay } from "react-bootstrap";
import { AiOutlineCheck, AiOutlineCloseCircle } from "react-icons/ai";

function AttackTypesOverlay(props) {
  const [show, setShow] = useState(false);
  const [target, setTarget] = useState(null);
  const ref = useRef(null);

  const onShowCard = (event) => {
    setShow(true);
    setTarget(event.target);
  };

  const onHideCard = () => {
    setShow(false);
  };

  const types = props.types.map((type, i) => (
    <div key={i}>
      <strong>{type}</strong>
    </div>
  ));

  const isSsh = props.types.length > 0 && props.types[0].includes("Found");
  return (
    <div ref={ref}>
      <Button
        variant={
          isSsh
            ? props.types[0].includes("password")
              ? "success"
              : "danger"
            : props.types.length > 0
            ? "danger"
            : "success disabled"
        }
        onMouseOver={onShowCard}
        onMouseOut={onHideCard}
      >
        {isSsh ? (
          props.types[0].includes("password") ? (
            <AiOutlineCheck />
          ) : (
            <AiOutlineCloseCircle />
          )
        ) : (
          props.types.length
        )}
      </Button>

      <Overlay
        show={show}
        target={target}
        placement="right"
        container={ref}
        containerPadding={20}
      >
        <Popover id="popover-contained">
          <Popover.Body>{types}</Popover.Body>
        </Popover>
      </Overlay>
    </div>
  );
}

export default AttackTypesOverlay;
