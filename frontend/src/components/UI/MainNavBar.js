import {Navbar, Container, Nav} from "react-bootstrap"
import { NavLink } from "react-router-dom";
import classes from './MainNavBar'
import LoginContext from "../../store/auth-context";
import axios from "axios";
import { useContext } from "react";

const NavBar = (props) => {
  const {isLogin, setIsLogin} = useContext(LoginContext)
  console.log(isLogin)

  const logoutHandler = (event) =>
  {
    event.preventDefault();
    axios.get('http://localhost:3001/auth/logout')
      .then(res => {
        console.log(res)
        if(res.data.logout === true)
        {
          setIsLogin(false)
        }
      })
      .catch((error) => {
        console.log(error)
        setIsLogin(true)
      })    
  }
  return (
    <Navbar bg="dark" className="mb-2" variant="dark" expand={true}>
      <Container>
        <Nav className="me-auto">
          {isLogin && <Nav.Link onClick={logoutHandler}>Logout</Nav.Link>}
          {!isLogin && <Nav.Link href="/login">Login</Nav.Link>}
          {!isLogin && <Nav.Link href="/register">Register</Nav.Link>}
        </Nav>
      </Container>
    </Navbar>
  );
};

export default NavBar;
