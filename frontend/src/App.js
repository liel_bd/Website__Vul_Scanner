import Attacks from "./pages/AttacksHandler";
import Register from "./pages/Register";
import Login from "./pages/Login";
import NotFound from "./pages/NotFound";

import { Route, Switch } from "react-router-dom";
import MainNavBar from "./components/UI/MainNavBar";
import { useState, useMemo } from "react";
import LoginContext from "./store/auth-context";

function App() {
  const [isLogin, setIsLogin] = useState(false);

  const value = useMemo(() => ({ isLogin, setIsLogin }), [isLogin]);

  return (
    <LoginContext.Provider value={value}>
      <header>
        <MainNavBar />
      </header>
      <main>
        <Switch>
          {/* <Route path="/" exact>

            </Route> */}
          <Route path="/register">
            <Register />
          </Route>
          <Route path="/login">
            <Login />
          </Route>
          <Route path="/attacks">
            <Attacks />
          </Route>
          <Route path="*">
            <NotFound />
          </Route>
        </Switch>
      </main>
    </LoginContext.Provider>
  );
}

export default App;
