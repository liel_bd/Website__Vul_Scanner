import { Fragment, useState, useCallback, useEffect, useRef } from "react";
import axios from "axios";
import { Alert } from "react-bootstrap";

import AttackNavbar from "../components/Attacks/UI/AttackNavbar";
import AttacksForm from "../components/Attacks/Forms/AttacksForm";
import AttacksList from "../components/Attacks/List/AttacksList";
import SshForm from "../components/Attacks/Forms/SshForm";
import SynFlood from "../components/Attacks/Forms/SynFlood";
import { Center } from "@chakra-ui/react";
import LoginContext from "../store/auth-context";
import { useContext } from "react";
import { Redirect } from "react-router-dom";

const Attacks = (props) => {
  const [formIsShown, setFormIsShown] = useState(false);
  const [sshFormIsShown, setSshFormIsShown] = useState(false);
  const [showFlash, setShowFlash] = useState(false);
  const {isLogin, setIsLogin} = useContext(LoginContext)

  const [attacks, setAttacks] = useState([]);

  const showFormHandler = () => {
    setFormIsShown(true);
  };
  const hideFormHandler = () => {
    setFormIsShown(false);
  };

  const showSshFormHandler = () => {
    setSshFormIsShown(true);
  };
  const hideSshFormHandler = () => {
    setSshFormIsShown(false);
  };

  useEffect(async () => {
    try {
      const url = "http://localhost:3001/attacks";
      const response = await fetch(url);
      if (!response.ok) {
        throw new Error("Something went wrong!");
      }

      const data = await response.json();
      console.log(data);
      setAttacks([...data.attacks]);
    } catch (error) {
      console.log(error);
    }
  }, []);

  const addAttackHandler = async (hostname) => {
    try {
      const url =
        "http://localhost:3001/attacks/checking/" +
        encodeURIComponent(hostname);
      console.log(url);
      const response = await fetch(url);
      if (!response.ok) {
        throw new Error("Something went wrong!");
      }

      const data = await response.json();
      if (data.isFind === false) {
        const tmpAttacks = [...attacks, data.attack];
        setAttacks(tmpAttacks);
      } else {
        setShowFlash(true);
      }
    } catch (error) {
      console.log(error);
    }
    console.log(attacks)
  };

  const sshScanHandler = async (hostname, username, port, file) => {
    const data = new FormData();

    data.append("file", file);
    data.append("hostname", hostname);
    data.append("username", username);
    data.append("port", port);

    const response = await axios.post("http://localhost:3001/attacks/ssh", data, {})
    const sshData = await response.data
    if (sshData.isFind === false) {
      const tmpAttacks = [...attacks, sshData.attack];
      setAttacks(tmpAttacks);
    } else {
      setShowFlash(true);
    }
  };

  return (
    <Fragment>
      {!isLogin && <Redirect to='/login'/>}
      <AttackNavbar
        onShowForm={showFormHandler}
        onShowSshForm={showSshFormHandler}
      />
      {formIsShown && (
        <AttacksForm
          onAddAttack={addAttackHandler}
          onCloseForm={hideFormHandler}
        />
      )}

      {sshFormIsShown && (
        <SshForm
          onSshScan={sshScanHandler}
          onCloseSshForm={hideSshFormHandler}
        />
      )}
      <SynFlood />
      <AttacksList items={attacks} />

      {showFlash && (
        <Center>
          <Alert
            className="w-50"
            variant="danger"
            onClose={() => setShowFlash(false)}
            dismissible
          >
            <p style={{ textAlign: "center" }}>
              You alredy search for this target.
            </p>
          </Alert>
        </Center>
      )}
    </Fragment>
  );
};

export default Attacks;
