import { Form, Button } from "react-bootstrap";
import { Center } from "@chakra-ui/react";
import Card from "../components/Auth/UI/Card";
import { GoAlert } from "react-icons/go";
import useInput from "../hooks/use-input";
import axios from "axios";
import LoginContext from "../store/auth-context";
import { useContext, useState } from "react";
import { Redirect } from "react-router-dom";
import { Alert } from "react-bootstrap";
import { Fragment } from "react";

const Login = () => {
  const { isLogin, setIsLogin } = useContext(LoginContext);
  const [redirect, setRedirect] = useState(null);
  const [showFlash, setShowFlash] = useState(false);

  const {
    value: enteredUsername,
    isValid: enteredUsernameIsValid,
    hasError: usernameInputHasError,
    valueChangeHandler: usernameChangeHandler,
    inputBlurHandler: usernameBlurHandler,
    reset: resetUsernameInput,
  } = useInput((value) => value.trim() != "");

  const {
    value: enteredPassword,
    isValid: enteredPasswordIsValid,
    hasError: passwordInputHasError,
    valueChangeHandler: passwordChangeHandler,
    inputBlurHandler: passwordBlurHandler,
    reset: resetPasswordInput,
  } = useInput((value) => value.trim() != "");

  const submitHandler = (event) => {
    event.preventDefault();

    if (!enteredUsernameIsValid || !enteredPasswordIsValid) {
      return;
    }

    const data = {
      username: enteredUsername,
      password: enteredPassword,
    };

    axios
      .post("http://localhost:3001/auth/login", data)
      .then((res) => {
        console.log(res)
        if (res.data.login) {
          setRedirect("/attacks");
          setIsLogin(true);
        } else {
          setIsLogin(false);
          setShowFlash(true);
        }
      })
      .catch((error) => {
        setIsLogin(false);
        setShowFlash(true);
      });
  };

  return (
    <Fragment>
      <Card>
        {isLogin && <Redirect to="/attacks" />}
        <Form onSubmit={submitHandler}>
          <Form.Group className="m-4" controlId="formBasicUsername">
            <Form.Label>Username</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter username"
              onChange={usernameChangeHandler}
              onBlur={usernameBlurHandler}
              value={enteredUsername}
            />
            {usernameInputHasError && (
              <div className="text-danger">
                <GoAlert /> Please enter a valid username.
              </div>
            )}
          </Form.Group>

          <Form.Group className="m-4" controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Password"
              onChange={passwordChangeHandler}
              onBlur={passwordBlurHandler}
              value={enteredPassword}
            />
            {passwordInputHasError && (
              <div className="text-danger">
                <GoAlert /> Please enter a valid password.
              </div>
            )}
          </Form.Group>

          <Center>
            <Button variant="success" type="submit">
              Submit
            </Button>
          </Center>
        </Form>
        {redirect ? <Redirect to={redirect} /> : ""}
      </Card>
      {showFlash && (
        <Center>
          <Alert
            className="w-50"
            variant="danger"
            onClose={() => setShowFlash(false)}
            dismissible
          >
            <p style={{ textAlign: "center"}}>
              Username or Password incorrect
            </p>
          </Alert>
        </Center>
      )}
    </Fragment>
  );
};

export default Login;
