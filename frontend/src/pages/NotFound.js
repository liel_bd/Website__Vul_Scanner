const NotFound = () => {
  return (
    <div
      style={{
        textAlign: "center",
        marginTop: "3rem",
        fontSize: "200%",
        color: "white",
      }}
    >
      <p>404: Page Not Found!</p>
    </div>
  );
};

export default NotFound;
