import { Form, Button } from "react-bootstrap";
import { Center } from "@chakra-ui/react";
import Card from "../components/Auth/UI/Card";
import useInput from "../hooks/use-input";
import { GoAlert } from "react-icons/go";
import { useState, useContext } from "react";
import validator from "validator";
import axios from "axios";
import { Redirect } from "react-router-dom";
import LoginContext from "../store/auth-context";

const Register = () => {
  const [redirect, setRedirect] = useState(null)
  const {isLogin, setIsLogin} = useContext(LoginContext)

  const {
    value: enteredUsername,
    isValid: enteredUsernameIsValid,
    hasError: usernameInputHasError,
    valueChangeHandler: usernameChangeHandler,
    inputBlurHandler: usernameBlurHandler,
    reset: resetUsernameInput,
  } = useInput((value) => value.trim() != "");

  const {
    value: enteredPassword,
    isValid: enteredPasswordIsValid,
    hasError: passwordInputHasError,
    valueChangeHandler: passwordChangeHandler,
    inputBlurHandler: passwordBlurHandler,
    reset: resetPasswordInput,
  } = useInput(
    (value) => value.trim() != ""
  );

  const {
    value: enteredEmail,
    isValid: enteredEmailIsValid,
    hasError: emailInputHasError,
    valueChangeHandler: emailChangeHandler,
    inputBlurHandler: emailBlurHandler,
    reset: resetEmailInput,
  } = useInput((value) => value.trim() && validator.isEmail(value));

  const submitHandler = (event) => {
    event.preventDefault();

    if (
      !enteredUsernameIsValid ||
      !enteredPasswordIsValid ||
      !enteredEmailIsValid
    ) {
      return;
    }

    const datails = {
      username: enteredUsername,
      password: enteredPassword,
      email: enteredEmail,
    };
    axios
      .post("http://localhost:3001/auth/register", datails)
      .then((res) => {
        if (res.data.auth) {
          setRedirect('/attacks')
          setIsLogin(true)
          console.log('liel')
          // props.onHide();
        } else {
          // setShow(true);
          setIsLogin(false)
        }
      })
      .catch((error) => {
        // setShow(true);
        setIsLogin(false)
        console.log(error);
      });
  };

  return (
    <Card>
      {isLogin && <Redirect to='/attacks'/>}
      <Form onSubmit={submitHandler}>
        <Form.Group className="m-4" controlId="formBasicUsername">
          <Form.Label>Username</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter username"
            onChange={usernameChangeHandler}
            onBlur={usernameBlurHandler}
            value={enteredUsername}
          />
          {usernameInputHasError && (
            <div className="text-danger">
              <GoAlert /> Please enter a valid username.
            </div>
          )}
        </Form.Group>

        <Form.Group className="m-4" controlId="formBasicPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            placeholder="Password"
            onChange={passwordChangeHandler}
            onBlur={passwordBlurHandler}
            value={enteredPassword}
          />
          {passwordInputHasError && (
            <div className="text-danger">
            <GoAlert /> Please enter a valid password.
          </div>
          )}
        </Form.Group>

        <Form.Group className="m-4" controlId="formBasicEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control
            type="email"
            placeholder="Enter email"
            onChange={emailChangeHandler}
            onBlur={emailBlurHandler}
            value={enteredEmail}
          />
          {emailInputHasError && (
            <div className="text-danger">
              <GoAlert /> Please enter a valid email.
            </div>
          )}
        </Form.Group>

        <Center>
          <Button variant="success" type="submit">
            Submit
          </Button>
        </Center>
      </Form>
      {redirect ? <Redirect to={redirect}/> : ''}
    </Card>
  );
};

export default Register;
