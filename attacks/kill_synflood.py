import wmi
import psutil


def main():
    kill_process()


def kill_process():
    f = wmi.WMI()
    for process in f.Win32_Process():
        if process.Name == 'python3.9.exe':
            p = psutil.Process(process.ProcessId)
            p.terminate()


if __name__ == "__main__":
    main()