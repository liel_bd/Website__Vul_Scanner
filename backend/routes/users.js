const express = require("express");
const router = express.Router();
const passport = require("passport");
const catchAsync = require("../utils/catchAsync");
const User = require("../models/user");

router.post(
  "/register",
  catchAsync(async (req, res, next) => {
    try {
      const { email, username, password } = req.body;
      const user = new User({ email, username });
      const registeredUser = await User.register(user, password);
      req.login(registeredUser, (err) => {
        if (err) return next(err);
        res.json({
          auth: true,
          username: username,
          msg: `Welcome ${username}!`,
        });
      });
    } catch (e) {
      res.json({
        auth: false,
        msg: e.message,
      });
    }
  })
);

router.post(
  "/login",
  passport.authenticate("local", { failureRedirect: "http://localhost:3001/" }),
  (req, res) => {
    res.json({
      login: true,
      username: global.currUser.username,
      msg: `Welcome ${global.currUser.username}!`,
    });
  }
);

router.get("/logout", (req, res) => {
  req.logout();
  res.json({
    logout: true,
    msg: "Goodbye!",
  });
});

module.exports = router;
