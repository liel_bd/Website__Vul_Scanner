const express = require("express");
const router = express.Router();
const Attack = require("../models/attack");
const dns = require("dns");
const attacksHandler = require("../handlers/attack");
const User = require("../models/user");
var formidable = require("formidable");
const fs = require("fs");
const {PythonShell} = require('python-shell');

let opPythonPath;
let opPath;
if (process.platform == "linux") {
  opPythonPath = "/usr/bin/python";
  opPath = "/home/ubadmin/Desktop/Website__Vul_Scanner/attacks/";
} else {
  opPythonPath =
    "C:\\Users\\A\\AppData\\Local\\Microsoft\\WindowsApps\\python.exe";
  opPath = "C:\\Users\\A\\Desktop\\Rafael\\Website__Vul_Scanner\\attacks";
}
const options = {
  mode: "text",
  pythonPath: opPythonPath,
  pythonOptions: ["-u"],
  scriptPath: opPath,
  args: null,
};

router.get("/", async (req, res) => {
  const user = await User.find({
    username: global.currUser.username,
  });
  const attacks = await Attack.find({ author: user[0]._id });
  res.json({
    attacks: attacks,
  });
});

router.get("/checking/:url", async (req, res) => {
  let { url } = req.params;
  url = decodeURI(url);

  const shortUrl = url.split("/")[2];
  console.log(global.currUser.username)
  const user = await User.find({
    username: global.currUser.username,
  });
  console.log(user)
  const findAttack = await Attack.find({
    $and: [{ author: user[0]._id }, { hostname: shortUrl }]
  });
  console.log(findAttack)
  if (findAttack.length) {
    res.json({
      isFind: true,
    });
  } else {
    options.args = [url];

    let attacks = [];
    attacks.push(await attacksHandler.xss(options));
    attacks.push(await attacksHandler.sqli(options));

    const date = attacksHandler.getDate();
    const types = attacks
      .map((atk) => {
        if (atk.vulnerable) {
          return atk.name;
        }
      })
      .filter((atk) => atk != undefined);

    dns.resolve4(shortUrl, async (err, addresses) => {
      const ip = addresses[0];
      const attack = new Attack({ hostname: shortUrl, ip, date, types });

      attack.author = user[0]._id;
      await attack.save();

      res.json({
        isFind: false,
        attack: attack,
      });
    });
  }
});

router.get("/synflood/start", async (req, res) => {
  options.args = [];
  PythonShell.run("synflood.py", options, (err, results) => {});
});

router.get("/synflood/stop", async (req, res) => {
  options.args = [];
  PythonShell.run("kill_synflood.py", options, (err, results) => {});
});

router.post("/ssh", async (req, res) => {
  const form = new formidable.IncomingForm();
  form.parse(req, async function (err, fields, files) {
    fs.readFile(files.file.filepath, "utf8", async (err, data) => {
      if (err) {
        console.error(err);
        return;
      }

      const { hostname, username, port } = fields;
      const user = await User.find({
        username: global.currUser.username,
      });
      const findAttack = await Attack.find({
        $and: [{ hostname: hostname }, { author: user[0]._id }],
      });
      if (findAttack.length) {
        res.json({
          isFind: true,
        });
      } else {
        options.args = [hostname, username, port, data];
        const sshRes = await attacksHandler.ssh(options);
        const types = [sshRes.password];
        const date = attacksHandler.getDate();
        dns.resolve4(hostname, async (err, addresses) => {
          const ip = addresses[0];
          const attack = new Attack({ hostname, ip, date, types });
          attack.author = user[0]._id;
          await attack.save();

          res.json({
            isFind: false,
            attack: attack,
          });
        });
      }
    });
  });
});

module.exports = router;
