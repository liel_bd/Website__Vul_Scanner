const mongoose = require('mongoose')
const Schema = mongoose.Schema

const AttackSchema = new Schema({
    hostname: {
        type: String,
        unique: false
    },
    ip: String,
    date: String,
    types: [String],
    author: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
})

module.exports = mongoose.model('Attack', AttackSchema)