const express = require('express')
const session = require('express-session')
const path = require('path')
const mongoose = require('mongoose')
const passport = require('passport')
const LocalStrategy = require('passport-local')
const methodOverride = require('method-override');
const User = require('./models/user');
const cors = require('cors');

const userRoutes = require('./routes/users');
const attackRoutes = require('./routes/attacks');
const bodyParser = require('body-parser')


mongoose.connect("mongodb://localhost:27017/cyber-project", {
  useNewUrlParser: true,
  //    useCreateIndex: true,
  useUnifiedTopology: true,
});
//connect to the mongoDB
const db = mongoose.connection;
//check for errors
db.on("error", console.error.bind(console, "connection error:"));
//open the connection
db.once("open", () => {
  console.log("Database connected");
});

//condig the express to a variable and set that it will parser the messages from the client
//as a JSON format
const app = express();
app.use(express.json());
app.use(cors());
const sessionConfig = {
  secret: "thisshouldbeabettersecret!",
  resave: false,
  saveUninitialized: true,
  cookie: {
    httpOnly: true,
    expires: Date.now() + 1000 * 60 * 60 * 24 * 7,
    maxAge: 1000 * 60 * 60 * 24 * 7,
  },
};

app.use(session(sessionConfig));

app.use(passport.initialize());
app.use(passport.session());
passport.use(new LocalStrategy(User.authenticate()));

passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

app.use((req, res, next) => {
  if(req.body.username){
    global.currUser = req.body;
  }
  next();
});

  

app.use('/auth', userRoutes);
app.use('/attacks', attackRoutes);

const port = 3001
app.listen(port, () => {
    console.log(`Listening on port ${port}...`)
})
