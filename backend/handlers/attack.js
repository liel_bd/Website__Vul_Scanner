const {PythonShell} = require('python-shell');

module.exports.getDate = () =>{
    let date_ob = new Date();
    let date = ("0" + date_ob.getDate()).slice(-2);
    let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
    let year = date_ob.getFullYear();
    return (year + "-" + month + "-" + date);
  }

//https://xss-game.appspot.com/level1/frame
module.exports.xss = async(options) => {
  const result = await new Promise((resolve, reject) => {
    PythonShell.run('xss.py', options, (err, results) => {
      if (err) return reject(err);
      const ans = {
        name: 'XSS(Cross Site Scripting)',
        vulnerable: results[0] === 'True'
      }
      return resolve(ans);
    });
  });
  return result;
} 

module.exports.sqli = async(options) => {
  const result = await new Promise((resolve, reject) => {
    PythonShell.run('sqli.py', options, (err, results) => {
      if (err) return reject(err);
      const isVul = results[1] === 'True'
      const ans = {
        name: isVul ? `SQL injection(in ${results[0].toUpperCase()})` : 'SQL injection',
        vulnerable: isVul
      }
      return resolve(ans);
    });
  });
  return result;
}

module.exports.ssh = async(options) => {
  const result = await new Promise((resolve, reject) => {
    PythonShell.run('bf_ssh.py', options, (err, results) => {
      if (err) return reject(err);
      const success = results[0] === 'True'
      const ans = {
        password: success ? `Found password: ${results[1]}` : 'Not Found',
      }
      return resolve(ans);
    });
  });
  return result;
}